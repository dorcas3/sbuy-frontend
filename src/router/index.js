import { createWebHistory, createRouter } from "vue-router";
import Login from "../components/Auth/Login.vue";
import Signup from "../components/Auth/Signup.vue";
import Home from "../components/Home.vue"
import Shop from "../components/pages/Shop.vue"
import PageNotFound from "../components/pages/PageNotFound.vue"
import Product_detail from "../components/pages/Product_detail.vue"
import Cart from '../components/pages/Cart.vue'
import Category from '../components/pages/Category.vue'
import Collection from '../components/pages/Collection.vue'
import Collection_detail from '../components/pages/Collection_detail.vue'

const routes = [
  
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/collections',
    name: 'Collection',
    component: Collection,
  },
  {
    path: '/collection-detail/:id/',
    name: 'Collection-detail',
    props: true,
    component: Collection_detail,
  },
  {
    path: "/shop",
    name: "Shop",
    component: Shop,
    props: true
  },
  {
    path: "/category/:id/",
    name: "Category",
    component: Category,
    props: true
  },
  {
    path: "/product-detail/:id",
    name: "Product-detail",
    props: true,
    component: Product_detail,
  },
  {
    path: "/cart",
    name: "Cart",
    props: true,
    component: Cart,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/signup",
    name: "Signup",
    component: Signup,
  },
  {
    path: '/:catchAll(.*)*',
    name: "PageNotFound",
    component: PageNotFound,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;