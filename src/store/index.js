import { createStore } from 'vuex';
import categories from './modules/categories'
import products from './modules/products'
import cart2 from './modules/cart2'
import subcategories from './modules/subCategories'
import auth from './modules/auth'

// Create store
const store = createStore({
    modules: {
      categories,
      products,
      cart2,
      subcategories,
      auth
    }
  });
  export default store;
  