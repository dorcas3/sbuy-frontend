import axios from 'axios'

const state = {
    cart: []
};

const getters = {
    ccart: state =>
        state.cart,
    cartTotalPrice: state => {
        let total = 0
        state.cart.forEach(item => {
            total += item.price * item.quantity
        });
        return total
    },
    cartItemCount: state => state.cart.length


}
const mutations = {
    addToCart(state, { product, quantity }) {
        let productInCart = state.cart.find(item => {
            return item.product_id === product.id
        });
        if (productInCart) {
            productInCart.quantity += quantity;
            return
        }

        state.cart.push({ product, quantity })
    },
    setCart(state, cartItems) {
        state.cart = cartItems
    }
}
const actions = {
    async addProductToCart({ commit }, { product, quantity }) {
        // let productInCart = state.cart.find(item => {
        //     return item.id === product.id
        // });
        // if (productInCart) {
        //     productInCart.quantity += quantity;
        //     return
        // }
        commit('addToCart', { product, quantity });
        axios.post('http://127.0.0.1:3333/api/v1/cart', { 
   
            product_id: product.id,
            p_name:product.p_name,
            price:product.price,
            quantity:quantity
           
        })
        alert('cart added successfully')
    },
    async getCartItems({ commit }) {
        axios.get('http://127.0.0.1:3333/api/v1/cart').then(response => {
            console.log('get cart items');
            console.log(response.data);
            commit('setCart', response.data)
        })
    }
}


export default {
    state,
    getters,
    actions,
    mutations
}