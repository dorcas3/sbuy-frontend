import axios from 'axios'


const state = {
    productsBySubCategory: [],
    products: [],
    product: []
};

const getters = {
    productsBySubCategory: state =>
        state.productsBySubCategory,
    allProducts: state =>
        state.products,
    oneProduct: state =>
        state.product,

}
const actions = {
    async fetchAllProducts({ commit }) {
        const response = await axios.get('http://127.0.0.1:3333/api/v1/products');
        commit('setProducts', response.data.data)
        
        // console.log(response.data.data);
    },
    async fetchOneProduct({ commit }, id) {
        const response = await axios.get(`http://127.0.0.1:3333/api/v1/products/${id}`);
        commit('setProduct', response.data.data)
        console.log('products',response.data);
    },
    async fetchProductsBySubCategory({ commit }, id) {
        const response = await axios.get(`http://127.0.0.1:3333/api/v1/products_by_subcategory/${id}`);
        commit('setProductsBySubCategory', response.data)
        console.log(response.data);
    },

}
const mutations = {
    setProducts: (state, products) => (state.products = products),
    setProduct: (state, product) => (state.product = product),
    setProductsBySubCategory: (state, productsBySubCategory) => (state.productsBySubCategory = productsBySubCategory),
}

export default {
    state,
    getters,
    actions,
    mutations
}