import axios from 'axios'


const state = {
    subCategories: [],
    subCategory:[],
   
};

const getters = {
    allsubCategories: state =>
        state.subCategories,
    singlesubCategory: state =>
        state.subCategory

}
const actions = {
    async fetchsubCategories({ commit }) {
        const response = await axios.get(' http://127.0.0.1:3333/api/v1/subcategories');
        commit('setsubCategories', response.data)
        console.log(response.data);
    },
    async fetchsubCategory({ commit },id) {
        const response = await axios.get(`http://127.0.0.1:3333/api/v1/subcategories/${id}`);
        commit('setsubCategory', response.data)
        console.log('setsub',response.data);
    },
}
const mutations = {
    setsubCategories: (state, subCategories) => (state.subCategories = subCategories),
    setsubCategory: (state, subCategory) => (state.subCategory = subCategory),
}

export default {
    state,
    getters,
    actions,
    mutations
}