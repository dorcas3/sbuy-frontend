import axios from 'axios'


const state = {
    categories: [],
    category: []
};

const getters = {
    allCategories: state =>
        state.categories,

    singleCategory: state =>
        state.category
}
const actions = {
    async fetchCategories({ commit }) {
        const response = await axios.get('http://127.0.0.1:3333/api/v1/categories');
        commit('setCategories', response.data)
        // console.log(response.data);
    },
    async fetchOneCategory({ commit }, id) {
        const response = await axios.get(`http://127.0.0.1:3333/api/v1/categories/${id}`)
        console.log(response.data);
        commit('setCategory', response.data)
    }
}
const mutations = {
    setCategories: (state, categories) => (state.categories = categories),
    setCategory: (state, category) => (state.category = category)
}

export default {
    state,
    getters,
    actions,
    mutations
}